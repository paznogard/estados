import 'package:estados/src/pages/contador_page.dart';
// ignore: unused_import
import 'package:estados/src/pages/home_page.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: ContadorPage());
  }
}
