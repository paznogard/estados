import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  final TextStyle estilo = TextStyle(fontSize: 30);
  int contador = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mi App'),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [Text('Cantidad de clicks', style: estilo), Text('$contador', style: estilo)],
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          print('Click en boton' + contador.toString());
          contador++;
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
