import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  @override
  _ContadorPageState createState() => _ContadorPageState();
}

class _ContadorPageState extends State<ContadorPage> {
  final TextStyle estilo = TextStyle(fontSize: 30);
  int contador = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mi Apps'),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [const Text('Cantidad de clicks'), Text('$contador', style: estilo)],
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          print('Click en boton' + contador.toString());
          contador++;
          setState(() {});
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
